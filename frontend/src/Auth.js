import React, { useState } from "react"
import { useFormik } from "formik";
import { useSignIn } from "react-auth-kit";
import axios, { AxiosError } from "axios";
import { useNavigate } from "react-router-dom";

function Auth() {

    window.axios = require('axios');

    const [error, setError] = useState();
    const [completed, setCompleted] = useState();

    const signIn = useSignIn();
    const navigate = useNavigate();

    const onSubmit = async (values) => {
        const { email, password, name} = values;
        if (authMode === "signin") {
            if (values.email === "" || values.password === "")
                setError("Some fields are blank!")
            else {
                try {
                    const response = await axios.post(
                        "http://localhost:4080/auth/login",{
                            email,
                            password
                        }
                    );

                    //localStorage.setItem('token', response.data.token);
                    console.log(response.data.token)
                    signIn({
                        token: response.data.token,
                        expiresIn: 3600,
                        tokenType: "Bearer",
                        authState: { email: values.email },
                    });
                    navigate({
                        pathname: "/",

                    });
                } catch (err) {
                    if (err.response.status === 401)
                        setError("Invalid email or password!")
                    else
                        setError("Something went wrong! Retry later")
                    //console.log("Error: ", err.response.data);
                }
            }
        }
        else {
            if (values.name === "" || values.email === "" || values.password === "")
                setError("Some fields are blank!")
            else {
                try {
                    const response = await axios.post(
                        "http://localhost:4080/auth/register",{
                            email,
                            password,
                            name
                        }
                    );
                    //console.log(response.toJSON());
                    //console.log(response)
                    setCompleted("Registration was successful, now you can sign in!")
                } catch (err) {
                    setError("Something went wrong! Retry later")
                }
            }
        }

    };
    const formik = useFormik({
        initialValues: {
            name: "",
            email: "",
            password: "",
        },
        onSubmit,
    });
    let [authMode, setAuthMode] = useState("signin")

    const changeAuthMode = () => {
        setAuthMode(authMode === "signin" ? "signup" : "signin")
        setError("");
        setCompleted("");
    }

    if (authMode === "signin") {
        return (
            <div className="Auth-form-container">
                <form className="Auth-form" onSubmit={formik.handleSubmit}>
                    <div className="Auth-form-content">
                        <h3 className="Auth-form-title">Sign In</h3>
                        <div className="text-center">
                            Not registered yet?{" "}
                            <span className="link-primary" onClick={changeAuthMode}>
                                Sign Up
                            </span>
                        </div>
                        <div className="form-group mt-3">
                            <label>Email address</label>
                            <input
                                name="email"
                                type="email"
                                className="form-control mt-1"
                                placeholder="Enter email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                            />
                        </div>
                        <div className="form-group mt-3">
                            <label>Password</label>
                            <input
                                name="password"
                                type="password"
                                className="form-control mt-1"
                                placeholder="Enter password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                            />
                        </div>
                        <div className="d-grid gap-2 mt-3">
                            <button type="submit" className="btn btn-primary">
                                Submit
                            </button>
                            {error ? <p className="Error">{error}</p> : null}
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    return (
        <div className="Auth-form-container">
            <form className="Auth-form" onSubmit={formik.handleSubmit}>
                <div className="Auth-form-content">
                    <h3 className="Auth-form-title">Sign Up</h3>
                    <div className="text-center">
                        Already registered?{" "}
                        <span className="link-primary" onClick={changeAuthMode}>
                            Sign In
                        </span>
                    </div>
                    <div className="form-group mt-3">
                        <label>Full Name</label>
                        <input
                            name="name"
                            type="text"
                            className="form-control mt-1"
                            placeholder="e.g Jane Doe"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                        />
                    </div>
                    <div className="form-group mt-3">
                        <label>Email address</label>
                        <input
                            name="email"
                            type="email"
                            className="form-control mt-1"
                            placeholder="Email Address"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                        />
                    </div>
                    <div className="form-group mt-3">
                        <label>Password</label>
                        <input
                            name="password"
                            type="password"
                            className="form-control mt-1"
                            placeholder="Password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                        />
                    </div>
                    <div className="d-grid gap-2 mt-3">
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                        {error ? <p className="Error">{error}</p> : null}
                        {completed ? <p className="Completed">{completed}</p> : null}
                    </div>
                </div>
            </form>
        </div>
    )
}
export { Auth };