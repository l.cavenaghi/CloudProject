import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"
import { BrowserRouter, Routes, Route } from "react-router-dom"
//import { Login } from "./components/";
import { Auth } from './Auth';
import { Home } from './Home';
//import { Home } from "./components/";
import { RequireAuth } from "react-auth-kit";
import { Recipe } from "./Recipe";
import { Myrecipes } from "./Myrecipes";
import { Shoplist } from "./Shoplist";


function App() {
  return (
    <Routes>
    <Route path="/auth" element={<Auth />}></Route>
    <Route
      path="/"
      element={
        <RequireAuth loginPath="/auth">
          <Home />
        </RequireAuth>
      }
    ></Route>
    <Route
      path="/recipe"
      element={
        <RequireAuth loginPath="/auth">
          <Recipe />
        </RequireAuth>
      }
    ></Route>
    <Route
      path="myrecipes"
      element={
        <RequireAuth loginPath="/auth">
          <Myrecipes />
        </RequireAuth>
      }
    ></Route>
    <Route
      path="shoplist"
      element={
        <RequireAuth loginPath="/auth">
          <Shoplist />
        </RequireAuth>
      }
    ></Route>
  </Routes>
  )
}

export default App