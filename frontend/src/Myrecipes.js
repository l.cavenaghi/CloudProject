import axios from "axios";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, React, useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import { useAuthUser } from 'react-auth-kit'

function Myrecipes() {
    const navigate = useNavigate();
    var firstCall = true;
    const [allRecipes, setAllRecipes] = useState([]);
    const [error, setError] = useState("");
    const [completed, setCompleted] = useState("");


    const back = () => {
        navigate("/");
    }

    const fetchMyRecipes = async () => {
        try {
            const response = await axios.get(
                "http://localhost:3080/all/recipe", { withCredentials: true }
            );
            console.log(response.data)
            setAllRecipes(response.data)
        } catch (err) {
            console.log(err.response)
            setError("Something went wrong")

        };
    }

    useEffect(() => {
        if (firstCall === true) {
            fetchMyRecipes();
            firstCall = false;
        }

    }, [""]);

    const deleteRecipe = async (recipeId) => {
        console.log(recipeId);
        try {
            /*
            const config = {
                headers: { Authorization: `Bearer ${token}` }
            };  */
            const response = await axios.delete(
                "http://localhost:3080/delete/recipe/" + recipeId, { withCredentials: true }
            );
            //console.log(response.data)
            setCompleted("Recipe deleted!")
            setTimeout(() => {
                setCompleted("");
            }, 2000);
            fetchMyRecipes();
        } catch (err) {
            console.log(err.response.data);
            setError("Something went wrong")
        }
    }











    return (
        <div className="Recipe-form-container">
            <form className="Recipe-form" onSubmit={e => { e.preventDefault(); }}>
                <div className="Auth-form-content">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-secondary mt-4" type="button" onClick={back}>Back</button>
                            </div>

                        </div>
                    </div>
                    <h1 className="Recipe-title">My recipes</h1>
                    <div className="content d-flex flex-column justify-content-center align-items-center">
                        <table className="table mb-5">
                            <thead>
                                <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Recipe</th>
                                    <th scope="col">People</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {allRecipes.map((value) => (
                                    <tr key={value.recipeId._id}>
                                        <td>			     
                                             <img src={value.recipeId.image} className="img-fluid img-thumbnail" width="70px" height="70px"/>
                                         </td>
                                        <td className="recipes">{value.recipeId.name}</td>
                                        <td className="recipes text-center">{value.people}</td>
                                        <td className="recipes text-center">
                                        <button className="btn btn-danger" type="button" onClick={() => deleteRecipe(value.recipeId._id)} >X</button >
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                        {allRecipes.length === 0 && (
                            <p>You have 0 recipes. Search a recipe and save it!</p>
                        )}
                        
                        {completed ? <p className="Completed mt-3 mb-5">{completed}</p> : null}
                        {error ? <p className="Error mt-3 mb-5">{error}</p> : null}
                    </div>
                </div>
            </form>
        </div>
    );
}

export { Myrecipes };