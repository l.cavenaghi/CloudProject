import axios from "axios";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, React, useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import { useAuthUser } from 'react-auth-kit'

function Recipe() {
    const authHeader = useAuthUser()
    const navigate = useNavigate();
    var firstCall = true;
    const id = useLocation();
    const [name, setName] = useState();
    const [img, setImg] = useState();
    const [url, setUrl] = useState();
    const [yields, setYield] = useState();
    const [source, setSource] = useState();
    const [ingredients, setIngredients] = useState([]);
    const [calories, setCalories] = useState();
    const [totalWeight, setTotalWeight] = useState();
    const [dietLabels, setDietLabels] = useState();
    const [healthLabels, setHealthLabels] = useState();

    const [selectedValue, setSelectedValue] = useState();
    const [originalIngredients, setOriginalIngredients] = useState([]);


    const [hasRecipe, setHasRecipe] = useState();
    const [completed, setCompleted] = useState("");
    const [error, setError] = useState("");


    const selectPeople = (num) => {
        setSelectedValue(num)

        const newList = originalIngredients.map((item) => {
            const updatedItem = {
                ...item,
                weight: item.weight / yields * num,
            };

            return updatedItem;

        });

        setIngredients([...newList]);
    }



    useEffect(() => {
        setSelectedValue("1")
        const fetchData = async () => {
            try {
                const response = await axios.get(
                    "http://localhost:3080/recipes/" + id.state.id, { withCredentials: true }
                );
                console.log(response.data)
                setName(response.data.name);
                setImg(response.data.image)
                setUrl(response.data.url)
                setYield(response.data.yield);
                setSource(response.data.source)
                setIngredients(response.data.ingredients)
                setCalories(response.data.calories);
                setTotalWeight(response.data.totalWeight)
                setDietLabels(response.data.dietLabels)
                setHealthLabels(response.data.healthLabels)


                setSelectedValue(response.data.yield);
                setOriginalIngredients(response.data.ingredients)
            } catch (err) {
                console.log(err.response)
                setError("Something went wrong")

            };
        }

        const fetchHasRecipe = async () => {
            try {
                const resp = await axios.get(
                    "http://localhost:3080/has-recipe/" + id.state.id, { withCredentials: true }
                );
                setHasRecipe(resp.data.hasRecipe)
            } catch (err) {
                console.log(err.response)
            }
        }
        if (firstCall === true) {
            fetchData();
            fetchHasRecipe();
            firstCall = false;
        }

    }, [""]);

    const back = () => {
        navigate("/");
    }

    const saveRecipe = async () => {
        try {
            console.log(authHeader().token);
            /*
            const config = {
                headers: { Authorization: `Bearer ${token}` }
            };  */
            const response = await axios.post(
                "http://localhost:3080/save/recipe", {
                id: id.state.id,
                name: name,
                image: img,
                url: url,
                yield: yields,
                source: source,
                ingredients: ingredients,
                calories: calories,
                totalWeight: totalWeight,
                dietLabels: dietLabels,
                healthLabels: healthLabels,
                people: selectedValue,
            }, {
                withCredentials: true
            }
            );
            setCompleted("Recipe saved!")
            setHasRecipe(true)
            setTimeout(() => {
                setCompleted("");
            }, 2000);
        } catch (err) {
            console.log(err.response.data);
            setError("Something went wrong")
        }
    }

    const deleteRecipe = async () => {
        try {
            /*
            const config = {
                headers: { Authorization: `Bearer ${token}` }
            };  */
            const response = await axios.delete(
                "http://localhost:3080/delete/recipe/" + id.state.id, { withCredentials: true }
            );
            //console.log(response.data)
            setCompleted("Recipe deleted!")
            setHasRecipe(false);
            setTimeout(() => {
                setCompleted("");
            }, 2000);
        } catch (err) {
            console.log(err.response.data);
            setError("Something went wrong")
        }
    }


    return (
        <div className="Recipe-form-container">
            <form className="Recipe-form" onSubmit={e => { e.preventDefault(); }}>
                <div className="Auth-form-content">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-secondary mt-4" type="button" onClick={back}>Back</button>
                            </div>

                        </div>

                    </div>
                    <h1 className="Recipe-title">{name}</h1>
                    <div className="content d-flex flex-column justify-content-center align-items-center">
                        <img src={img} className="rounded mx-auto d-block"></img>
                        <h1 className="Auth-form-title mt-4">Ingredients:</h1>

                        <table className="table mb-5">
                            <thead>
                                <tr>
                                    <th scope="col">Ingredient</th>
                                    <th scope="col">Quantity (g) for {selectedValue} people</th>
                                </tr>
                            </thead>

                            <tbody>

                                {ingredients.map((value) => (


                                    <tr key={value.text + value.weight.toFixed(2)}>
                                        <td>{value.text}</td>
                                        <td>{value.weight.toFixed(2)} g</td>
                                    </tr>

                                ))}

                            </tbody>


                        </table>



                        <Dropdown>
                            <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                Number of people: {selectedValue}
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                <Dropdown.Item onClick={() => selectPeople("1")}>1</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("2")}>2</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("3")}>3</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("4")}>4</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("5")}>5</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("6")}>6</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("7")}>7</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("8")}>8</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("9")}>9</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("10")}>10</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("11")}>11</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("12")}>12</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("13")}>13</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("14")}>14</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("15")}>15</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("16")}>16</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("17")}>17</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("18")}>18</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("19")}>19</Dropdown.Item>
                                <Dropdown.Item onClick={() => selectPeople("20")}>20</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        {hasRecipe && (
                            <button className="btn btn-danger mt-3 mb-4" type="button" onClick={deleteRecipe}>Delete recipe</button>
                        )}
                        {!hasRecipe && (
                            <button className="btn btn-primary mt-3 mb-4" type="button" onClick={saveRecipe}>Save recipe</button>
                        )}



                        {completed ? <p className="Completed">{completed}</p> : null}
                        {error ? <p className="Error">{error}</p> : null}
                        <div className="container mb-5">

                        </div>



                    </div>

                </div>

            </form>


        </div>
    );
}

export { Recipe };