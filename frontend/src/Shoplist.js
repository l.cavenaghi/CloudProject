import axios from "axios";
 import { useNavigate } from "react-router-dom";
 import { useEffect, React, useState } from 'react'

 function Shoplist() {
     const navigate = useNavigate();
     var firstCall = true;
     const [shopList, setShopList] = useState([]);
     const [error, setError] = useState("");


     const back = () => {
         navigate("/");
     }

     const fetcShopList = async () => {
         try {
             const response = await axios.get(
                 "http://localhost:3080/all/recipe/function", { withCredentials: true }
             );
             console.log(response.data)
             setShopList(response.data)
         } catch (err) {
             console.log(err.response)
             setError("Something went wrong")

         };
     }

     useEffect(() => {
         if (firstCall === true) {
             fetcShopList();
             firstCall = false;
         }

     }, [""]);

     return (
         <div className="Recipe-form-container">
             <form className="Recipe-form" onSubmit={e => { e.preventDefault(); }}>
                 <div className="Auth-form-content">
                     <div className="container">
                         <div className="row">
                             <div className="col">
                                 <button className="btn btn-outline-secondary mt-4" type="button" onClick={back}>Back</button>
                             </div>

                         </div>
                     </div>
                     <h1 className="Recipe-title">My shop list</h1>
                     <div className="content d-flex flex-column justify-content-center align-items-center">
                         <table className="table mb-5">
                             <thead>
                                 <tr>
                                     <th scope="col">Ingredient</th>
                                     <th scope="col">Quantity</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 {shopList.map((value) => (
                                     <tr key={value._id}>
                                         <td className="recipes text-center">{value.text}</td>
                                         <td className="recipes text-center">{value.weight.toFixed(2)} g</td>
                                     </tr>
                                 ))}
                             </tbody>
                         </table>

                         {shopList.length === 0 && (
                             <p>You have 0 ingredients to buy. Search a recipe and save it!</p>
                         )}
                         {error ? <p className="Error mt-3 mb-5">{error}</p> : null}
                     </div>
                 </div>
             </form>
         </div>
     );
 }

 export { Shoplist };