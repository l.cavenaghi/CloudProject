import axios from "axios";
import { useSignOut } from "react-auth-kit";
import { useNavigate } from "react-router-dom";
import React from 'react'
import SearchBar from "./Components/SearchBar";
import Spinner from 'react-bootstrap/Spinner';

function Home() {
  const singOut = useSignOut();
  const navigate = useNavigate();

  const logout = () => {
    singOut();
    navigate("/auth");
  };

  const myRecipes = () => {
    navigate("/myrecipes");
  }

  const shoplist = () => {
    navigate("/shoplist");
  }

  return (
    <div className="Auth-form-container">
      <div className="Home-form">
        <div className="Auth-form-content">
          <h1 className="Home-form-title">Welcome!</h1>
          <div className="container">
            <div className="row justify-content-md-center mt-3 mb-5">
              <div className="col" align="center">
                <button className="btn btn-primary" onClick={myRecipes}>My recipes</button>
              </div>
              <div className="col" align="center">
              <button className="btn btn-primary" onClick={shoplist}>My shop list</button>
              </div>
              <div className="col" align="center">
                <button className="btn btn-primary" onClick={logout}>Logout</button>
              </div>
            </div>
          </div>
          <SearchBar placeholder="Enter a Recipe Name..." />

        </div>
      </div>
    </div>
  );
}

export { Home };