import React, { useState } from "react";
import { createSearchParams, Link, useNavigate } from "react-router-dom";
import axios, { AxiosError } from "axios";
import "./SearchBar.css";

function SearchBar({ placeholder, data }) {
  const navigate = useNavigate();
  const [filteredData, setFilteredData] = useState([]);
  const [noRecipe, setNoRecipe] = useState();
  const [wordEntered, setWordEntered] = useState("");
  const [loading, setLoading] = useState();

  const openRecipe = (id) => {
    navigate("/recipe", {
      state: {
        id: id
      }
    });
  };
  const handleInput = (event) => {
    const searchWord = event.target.value;
    setWordEntered(searchWord);
    if (searchWord.length === 0) {
      setFilteredData("");
      setNoRecipe(false);
    }
  }

  const _handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleFilter();
    }
  }

  const handleFilter = async () => {
    try {
      setLoading(true);
      const response = await axios.get(
        "http://localhost:3080/recipes?q=" + wordEntered
      );
      console.log(response.data[0].name)
      setFilteredData(response.data)
    } catch (err) {
      setNoRecipe(true)
      console.log(err.response)
    };
    setLoading(false);
  }

  const clearInput = () => {
    setFilteredData([]);
    setWordEntered("");
  };

  return (
    <div>
      <div className="content d-flex flex-column justify-content-center align-items-center">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col">
              <input
                className="form-control"
                type="text"
                placeholder={placeholder}
                value={wordEntered}
                onChange={handleInput}
                onKeyDown={_handleKeyDown}
              />
            </div>
            <div className="col col col-lg-2">
              <button className="btn btn-outline-secondary" type="button" onClick={handleFilter}>Search</button>
            </div>
          </div>
          <div className="row justify-content-md-center mt-3">
            <div className="col">
              <div className="d-flex justify-content-center">
                {loading === true &&
                  <div className="spinner-border text-primary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                }

              </div>
              {filteredData.length != 0 && (
                <div className="list-group">
                  {filteredData.slice(0, 10).map((value) => {
                    return (
                      <a className="list-group-item list-group-item-action" key={value.id} onClick={() => openRecipe(value.id)} target="_blank">
                        {value.name}
                      </a>
                    );
                  })}
                </div>
              )}
              {noRecipe && filteredData.length == 0 && (
                <ul className="list-group">
                  <a target="_blank">
                    <li className="list-group-item">There are 0 recipes with this name, try again!</li>
                  </a>
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SearchBar;