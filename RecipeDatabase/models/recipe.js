const mongoose = require('mongoose');

const recipeSchema = new mongoose.Schema({  
  _id: {
    type: String,
    required: true
  },                                                                                               
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  yield: {
    type: Number,
    required: true
  },
  source: {
    type: String,
    required: true
  },
  ingredients: [{
    text: {
      type: String,
      required: true
    },
    weight: {
      type: Number,
      required: true
    }
  }],
  calories: {
    type: Number,
    required: true
  },
  totalWeight: {
    type: Number,
    required: true
  },
  dietLabels: [{
    type: String
  }],
  healthLabels: [{
    type: String
  }]
}, { timestamps: true });

const Recipe = mongoose.model('Recipe', recipeSchema);

module.exports = Recipe;