const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Recipe = require('../models/recipe');
const jwt = require('jsonwebtoken')

// Verify JWT middleware
const verifyJWT = (req, res, next) => {
  try {
    // Get the token from the authorization header - will be inserted into the client request nel campo authorization dell'header
    const token = req.headers.authorization.split(' ')[1];
    // Verify the token using the secret key
    const decoded = jwt.verify(token, 'secret-key');
    // Add the decoded token to the request object
    req.decoded = decoded;
    // Call the next middleware function
    next();
  } catch (error) {
    // If the token is invalid, return a 401 Unauthorized error
    res.status(401).json({ error: 'Unauthorized' });
  }
};

// Get all users (non serve, é una prova)
router.get('/users', (req, res) => {
    User.find()
      .then(users => res.json(users))
      .catch(err => res.status(400).json(err));
});
  
// Get a single user by id
router.get('/users/:id', (req, res) => {
    User.findOne({ _id: req.params.id })
      .then(user => res.json(user))
      .catch(err => res.status(400).json(err));
});

// Get a single user by id
router.get('/users/login/:email', (req, res) => {
  User.findOne({ email: req.params.email })
    .then(user => res.json(user))
    .catch(err => res.status(400).json(err));
});
  
// Create a new user
router.post('/users', (req, res) => {
  // Check if the email is already in use
  User.findOne({ email: req.body.email })
    .then(user => {
      if (user) {
        return res.status(400).json({ error: 'Email is already in use' });
      }

      // If the email is not in use, create a new user
      const newUser = new User({
        email: req.body.email,
        password: req.body.password,
        name: req.body.name
      });
  
      newUser.save()
        .then(user => res.json(user))
        .catch(err => res.status(400).json(err));
    })
    .catch(err => res.status(400).json(err));
});

// Update an existing user by id
router.put('/users/:id', (req, res) => {
    User.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(user => res.json(user))
      .catch(err => res.status(400).json(err));
});

// Delete a user by id
router.delete('/users/:id', (req, res) => {
    User.findOneAndDelete({ _id: req.params.id })
      .then(() => res.json({ success: true }))
      .catch(err => res.status(400).json(err));
});

// Get all recipes associated with the user making the request (triggered by Faas)
router.get('/user/recipes/:uid', (req, res) => {
  User.findById(req.params.uid)
    .populate('recipes.recipeId')
    .then(user => res.json(user.recipes)) // Send the populated recipes array as the response
    .catch(err => res.status(400).json(err));
});

// Create a new recipe
router.post('/recipes', (req, res) => {
  // Check if the recipe already exists in the Recipe collection
  Recipe.findOne({ _id: req.body._id })
    .then(recipe => {
      if (!recipe) {
        // If the recipe does not exist, save it
        const newRecipe = new Recipe({
          _id: req.body._id,
          name: req.body.name,
          image: req.body.image,
          url: req.body.url,
          yield: req.body.yield,
          source: req.body.source,
          ingredients: req.body.ingredients,
          calories: req.body.calories,
          totalWeight: req.body.totalWeight,
          dietLabels: req.body.dietLabels,
          healthLabels: req.body.healthLabels,
        });
        newRecipe.save()
          .then(savedRecipe => {
            // Add the saved recipe's reference and the people number to the user's recipes array
            User.findOneAndUpdate({ _id: req.body.uid }, { $push: { recipes: { recipeId: savedRecipe._id, people: req.body.people } } })
              .then(() => res.json(savedRecipe))
              .catch(err => res.status(400).json(err));
          })
          .catch(err => res.status(400).json(err));
      } else {
        // If the recipe already exists, add its reference and the people number to the user's recipes array
        User.findOneAndUpdate({ _id: req.body.uid }, { $push: { recipes: { recipeId: recipe._id, people: req.body.people } } })
          .then(() => res.json(recipe))
          .catch(err => res.status(400).json(err));
      }
    })
    .catch(err => res.status(400).json(err));
});

// Delete a recipe by id
router.delete('/recipes/:id', (req, res) => {
  // Find the user who made the request using the id in the JWT
  User.findOneAndUpdate({ _id: req.body.uid }, { $pull: { recipes: { recipeId: req.params.id } } })
    .then(() => res.json({ success: true }))
    .catch(err => res.status(400).json(err));
});

// Check the presence of a recipe for a user
router.get('/has-recipe/:userId/:recipeId', (req, res) => {
  User.findOne({ _id: req.params.userId }, (err, user) => {
    if (err) {
      res.send(500, 'Error fetching user');
    } else if (user) {
      let hasRecipe = false;
      for (let i = 0; i < user.recipes.length; i++) {
        if (user.recipes[i].recipeId == req.params.recipeId) {
          hasRecipe = true;
          break;
        }
      }
      res.send(200, { hasRecipe: hasRecipe });
    } else {
      res.send(404, 'User not found');
    }
  });
});

module.exports = router;
