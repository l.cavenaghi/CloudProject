const express = require('express')
const bodyParser = require('body-parser');
const port = 5080;
const app = express();
const mongoose = require("mongoose");
const urlDB = "mongo-db.default.svc.cluster.local:27017";
const userRecipeRrouter = require('./routes/router');

app.use(bodyParser.json());

mongoose.connect('mongodb://'+urlDB+'/microserviceapp', { useNewUrlParser: true }).then(() => {
    console.log("DB connected")
}).catch((err)=> {
    console.log("ERROR")
})

// Route
app.use('/api', userRecipeRrouter);

// Server listner
app.listen(port, () => {
  console.log(`App opened at port > ${port}!`)
});

