// server.js

const express = require('express');
const app = express();
const axios = require('axios');
const request = require('request');
const console = require('console');
var cors = require('cors')
const cookieParser = require("cookie-parser");
const jwt = require('jsonwebtoken')

app.use(
  cors({
    credentials: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: ['Content-Type', 'Authorization'],
    origin: ['http://localhost:3000'], // whatever ports you used in frontend
  })
);
app.use(cookieParser());
app.use(express.json());

// key and id for edamam API
const appId = '22979c9b';
const appKey = '59de1927c2d6dc88d1db6ca50356d8a1';

const verifyJWT = (req, res, next) => {
  try {
    // Get the token from the authorization header - will be inserted into the client request nel campo authorization dell'header
    //const token = req.headers.authorization.split(' ')[1];
    //console.log(req.headers.cookie);
    const token = req.cookies._auth;
    // Verify the token using the secret key
    const decoded = jwt.verify(token, 'secret-key');
    // Add the decoded token to the request object
    req.decoded = decoded;
    // Call the next middleware function
    next();
  } catch (error) {
    console.log(error)
    // If the token is invalid, return a 401 Unauthorized error
    res.status(401).json({ error: 'Unauthorized' });
  }
};

app.get('/recipes', (req, res) => {
  // build the URL for the Edamam API request
  // using the query parameters from the HTTP request
  const query = req.query;
  const url = `https://api.edamam.com/api/recipes/v2?type=public&q=${encodeURIComponent(query.q)}&app_id=${appId}&app_key=${appKey}`; // &diet=${encodeURIComponent(query.diet)}&health=${encodeURIComponent(query.health)}&cuisineType=${encodeURIComponent(query.cuisineType)}&mealType=${encodeURIComponent(query.mealType)}&dishType=${encodeURIComponent(query.dishType)}&calories=${encodeURIComponent(query.calories)}`;

  // send the HTTP request to the Edamam server
  request(url, { json: true }, (error, response, body) => {
    if (error) {
      // handle any errors
      res.status(500).send(error);
    } else {
      // parse the response from the API
      const recipes = body.hits; // this is an array of recipe objects
      const parsedRecipes = recipes.map(recipe => {
        // extract the information you need from each recipe object
        return {
          id: recipe.recipe.uri.split('#')[1],
          name: recipe.recipe.label,
          image: recipe.recipe.image
        }
      });
      // send the parsed response to the client
      res.send(parsedRecipes);
    }
  });
});

app.get('/recipes/:id', (req, res) => {
  const id = req.params.id;
  const url = `https://api.edamam.com/api/recipes/v2/${id}?app_id=${appId}&app_key=${appKey}&type=public`;

  request(url, { json: true }, (error, response, body) => {
    if (error) {
      res.status(500).send(error);
    } else {
      // parse the response from the API
      const recipe = body.recipe;
      const parsedRecipe = {
        id: recipe.uri.split('#')[1],
        name: recipe.label,
        image: recipe.image,
        url: recipe.url,
        yield: recipe.yield,
        source: recipe.source,
        ingredients: recipe.ingredients.map(ingredient => {
          return {
            text: ingredient.text,
            weight: ingredient.weight
          }
        }),
        calories: recipe.calories,
        totalWeight: recipe.totalWeight,
        dietLabels: recipe.dietLabels,
        healthLabels: recipe.healthLabels
      };
      // send the parsed response to the client
      res.send(parsedRecipe);
    }
  });
});

// Save new recipe for the user
app.post('/save/recipe', verifyJWT, async (req, res) => {
  const { id, name, image, url, yield, source, ingredients, calories, totalWeight, dietLabels, healthLabels, people} = req.body;
  try{
    const { data: response } = await axios.post('http://db-handler:5080/api/recipes', { 
      _id: id,
      name: name,
      image: image,
      url: url,
      yield: yield,
      source: source,
      ingredients: ingredients,
      calories: calories,
      totalWeight: totalWeight,
      dietLabels: dietLabels,
      healthLabels: healthLabels,
      people: people,
      uid: req.decoded.id
    })
    res.json(response)
  } catch (error){
    res.status(400).json({error: error.message});
  }
});

// Delete recipe from id
app.delete('/delete/recipe/:id', verifyJWT, async (req, res) => {
  const id =  req.params.id;
  console.log(id)
  try{
    const { data: response } = await axios.delete(`http://db-handler:5080/api/recipes/${id}`, {
      data: {uid: req.decoded.id}
    })
    res.json(response)
  } catch (error){
    res.status(400).json({error: error.message});
  }
})

// Has a recipe from id
app.get('/has-recipe/:id', verifyJWT, async (req, res) => {
  const id =  req.params.id;
  const uid = req.decoded.id;
  const uri = `http://db-handler:80/api/has-recipe/${uid}/${id}`
  console.log(uri)
  try{
    const {data: response} = await axios.get(`http://db-handler:5080/api/has-recipe/${uid}/${id}`);
    console.log(response)
    res.json(response);
  } catch (error){
    res.status(400).json({error: error.message});
  }
})

// All recipes of a user
app.get('/all/recipe', verifyJWT, async (req, res) => {
  const uid = req.decoded.id;
  try{
    const { data: response } = await axios.get(`http://db-handler:5080/api/user/recipes/${uid}`);
    res.json(response);
  } catch (error){
    res.status(400).json({error: error.message});
  }
})

// Pass the uid to FaaS
app.get('/all/recipe/function', verifyJWT, async (req, res) => {
  const uid = req.decoded.id;
  try{
    const { data: response } = await axios.post('http://gateway.openfaas.svc.cluster.local:8080/function/faas', { uid: uid });
    res.json(response);
  } catch (error){
    res.status(400).json({error: error.message});
  }
});

const port = 3080;
app.listen(port, () => {
  console.log(`Server in ascolto sulla porta ${port}`);
});