"use strict"
const axios = require('axios');

module.exports = async (event, context) => {
    const res = JSON.parse(event)
    const uid = res.uid
    const response = await axios.get(`http://db-handler.default.svc.cluster.local:5080/api/user/recipes/${uid}`);
    const recipes = response.data;

    let ingredients = [];
    for (let i = 0; i < recipes.length; i++) {
        const recipe = recipes[i].recipeId;
        const people = recipes[i].people;
        const recipeYield = recipe.yield;
        const proportion = people / recipeYield;
        for (let j = 0; j < recipe.ingredients.length; j++) {
            const ingredient = recipe.ingredients[j];
            ingredient.weight = ingredient.weight * proportion;
            ingredients.push(ingredient);
        }
    }
    return ingredients;
}
