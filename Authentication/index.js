const express = require('express');
const app = express();
const axios = require('axios');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
var cors = require('cors')

app.use(express.json());
app.use(cors())

// Registration
app.post('/auth/register', async (req, res) => {
  const { email, password, name} = req.body;

  try {
    // Hash the password
    const hash = await bcrypt.hash(password, 10);

    // Send a request to the database microservice to create a new user
    const { data: user } = await axios.post('http://db-handler:5080/api/users', {
      email,
      password: hash,
      name
    });

    // Return the created user
    res.json(user);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// Login
app.post('/auth/login', async (req, res) => {
    const { email, password } = req.body;

    try {
      // Send a request to the database microservice to get the user with the given email
      const { data: user } = await axios.get(`http://db-handler:5080/api/users/login/${email}`);
  
      // Verify that the provided password matches the one stored in the database
      if(user !== null){
        const isValid = await bcrypt.compare(password, user.password);
        if (!isValid) {
          return res.status(401).json({ error: 'Invalid email or password' });
        }
      }
      else
        return res.status(401).json({ error: 'Invalid email or password' });
  
      // Create a JWT containing the user's email and id
      const token = jwt.sign({ email: user.email, id: user._id}, 'secret-key');
  
      // Return the JWT
      res.json({ token });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

app.listen(4080, () => {
  console.log('Auth service listening on port 4800');
});
